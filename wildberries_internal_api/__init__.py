from .authentication import *
from .seller import *
from .buyer import *


__version__ = "0.110"
__author__ = "Fomchenkov Vyacheslav"
__email__ = "fomchenkov.dev@gmail.com"
__description__ = "WB internal API"
__url__ = "https://gitlab.com/fomch/wildberries_internal_api"
