import os
import io
import json
import time
import base64
import pickle
import warnings
import urllib.parse
from typing import Optional

import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service


class WBSellerAuth:
	def __init__(self, phone_number=None, session_file=None, sessions_folder="wb_sessions", headless=True, initialize_driver=True):
		self.phone_number = phone_number or input("Введите номер телефона: ")

		self.wb_input_phone = self.phone_number
		if self.wb_input_phone.startswith('7'):
			self.wb_input_phone = self.phone_number[1:]

		self.sessions_folder = sessions_folder
		try:
			os.makedirs(self.sessions_folder, exist_ok=True)
		except Exception as e:
			pass
		self.session_file = os.path.join(self.sessions_folder, session_file or f"7{self.wb_input_phone}")
		self.headless = headless
		if initialize_driver:
			self.initialize_driver()

	def initialize_driver(self):
		options = webdriver.ChromeOptions()
		print(self.headless)
		if self.headless:
			options.add_argument('--headless')

		options.add_argument('--no-sandbox')
		options.add_argument('--disable-gpu')
		self.driver = Chrome(options=options)
		self.driver.set_window_size(400, 800)

	def get_cookies_dict(self):
		url = 'https://seller.wildberries.ru/'

		self.open_page(url)

		cookies = self.driver.get_cookies()
		cookies_dict = {cookie['name']: cookie['value'] for cookie in cookies}

		return cookies_dict
	
	@staticmethod
	def convert_cookie_to_requestscookie(cookie:dict):
		session = requests.Session()
		session.cookies.update(cookie)
		return session.cookies
	
	def save_cookies(self, to_pickle=True):
		cookie_browser = self.get_cookies_dict()
		
		if not to_pickle:
			json_data = json.dumps(cookie_browser, indent=2)
			with open(self.session_file, 'w') as json_file:
				json_file.write(json_data)
			return
		
		requests_cookies = self.convert_cookie_to_requestscookie(cookie_browser)
		with open(self.session_file, 'wb') as f:
			pickle.dump(requests_cookies, f)

	def open_page(self, url):
		return self.driver.get(url)

	def check_session(self):
		url = "https://seller.wildberries.ru/"
		self.open_page(url)
		time.sleep(5)
		return 'auth' not in self.driver.current_url

	def send_number_phone(self):
		self.open_page("https://seller-auth.wildberries.ru/")
		time.sleep(3)
		WebDriverWait(self.driver, 10).until(ec.presence_of_element_located((By.TAG_NAME, "html")))
		self.enter_phone_number()
		captcha_path = self.capture_captcha()
		print(f"Капча сохранена: {captcha_path}")
		return captcha_path

	def enter_phone_number(self):
		input_phone = self.driver.find_element(By.XPATH, "//input[@placeholder='999 999-99-99']")

		input_phone.send_keys(self.wb_input_phone)
		input_phone.send_keys(Keys.ENTER)
		time.sleep(1)
		error = self.check_error_phone_number()
		if error:
			raise ValueError(f"Error: {error}")

	def capture_captcha(self):
		time.sleep(2)
		xpath = '//div[contains(@class, "CaptchaFormContentView")]//img'
		# Проверяем, появилось ли окошко с капчей
		captcha_element = self.driver.find_element(By.XPATH, xpath)
		return captcha_element.get_attribute('src')

	def send_captcha(self, captcha_value=None):
		captcha_value = captcha_value or input("Введите капчу: ")

		xpath = '//div[contains(@class, "CaptchaFormContentView")]//form//input'
		input_captcha = self.driver.find_element(By.XPATH, xpath)
		input_captcha.send_keys(captcha_value)
		input_captcha.send_keys(Keys.ENTER)
		time.sleep(2)

		if self.is_invalid_captcha(xpath):
			raise ValueError("Неверная капча")
		
		return True

	def is_invalid_captcha(self, selector):
		try:
			self.driver.find_element(By.CSS_SELECTOR, selector)
			return True
		except Exception:
			return False
		
	def check_error_phone_number(self):
		selector = "#gatsby-focus-wrapper > div > div > div.LoginFormView__input > form > div > div > div > span"
		try:
			return self.driver.find_element(By.CSS_SELECTOR, selector).text
		except Exception:
			return False 

	def send_verify_code(self, code=None):
		if not code:
			code = input("Введите код подтверждения: ")
		selector = "#Portal-modal > div > div > div > div > div > div > div.CodeInputContentView > div.CodeInputContentView__code-wrapper > form > div > ul > li:nth-child(1) > input"
		input_code = self.driver.find_element(By.CSS_SELECTOR, selector)
		input_code.send_keys(code)
		time.sleep(2)
		if 'auth' in self.driver.current_url:
			raise ValueError("Неверный код")
		self.save_cookies()
		print("Сессия сохранена предварительно.")
		print("Дождитесь 10 секунд для сохранеия полной версии куки")
		time.sleep(10)
		self.save_cookies()
		return True


class WBBuyerAuth():
	def __init__(self, phone_number=None, sessions_path='sessionsfl', headless=True, debug_mode=False) -> None:
		self.phone_number = phone_number or input("Введите номер телефона: ")

		self.wb_input_phone = self.phone_number
		if self.input_phone.startswith('7'):
			self.wb_input_phone = self.phone_number[1:]

		self.headless = headless
		self.sessions_path = sessions_path
		self.debug_mode = debug_mode

		self.default_headers = {
			'authority': 'www.wildberries.ru',
			'accept': '*/*',
			'accept-language': 'ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6',
			'content-length': '0',
			'origin': 'https://www.wildberries.ru',
			'referer': 'https://www.wildberries.ru/',
			'sec-ch-ua': '"Chromium";v="118", "Google Chrome";v="118", "Not=A?Brand";v="99"',
			'sec-ch-ua-mobile': '?0',
			'sec-ch-ua-platform': '"Linux"',
			'sec-fetch-dest': 'empty',
			'sec-fetch-mode': 'cors',
			'sec-fetch-site': 'same-origin',
			'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
			'x-requested-with': 'XMLHttpRequest',
			'x-spa-version': '9.3.143.3',
		}
		
		if not os.path.exists(self.sessions_path):
			os.makedirs(self.sessions_path)

		self.session_path = os.path.join(self.sessions_path, self.phone_number)

		path_exists = self.check_sesssion_path_exists()
		self.session = requests.Session()

		if path_exists:
			self._load_session()

	def check_sesssion_path_exists(self):
		if os.path.exists(self.session_path):
			return True
		warnings.warn("Отсутствует файл сессии")
		return False

	def check_session_valid(self):
		url = 'https://www.wildberries.ru/webapi/basket/info'
		headers = self.default_headers

		response = self.session.post(url, headers=headers)
		response_data = response.json()
		is_auth = response_data['IsAuthenticated']
		if is_auth:
			self._load_session()
		return is_auth

	def check_authorization(self):
		check_session_path = self.check_sesssion_path_exists()

		if not check_session_path:
			return False
		
		session_valid = self.check_session_valid()
		return session_valid

	@staticmethod
	def _convert_cookies_to_dict_for_pickle(driver:webdriver.Chrome):
		cookies = {x['name']: x['value'] for x in driver.get_cookies()}
		return cookies
	
	def _save_session(self):
		with open(self.session_path, 'wb') as f:
			pickle.dump(self.session.cookies, f)

	def _load_session(self):
		with open(self.session_path, 'rb') as f:
			self.session.cookies.update(pickle.load(f))
			bearer_token = self.session.cookies.get('authorization')
			self.default_headers['authorization'] = bearer_token

	def check_captcha_exist(self):
		try:
			captcha_image = self.driver.find_element(By.CSS_SELECTOR, "#spaAuthForm > div > div.form-block.form-block--captcha > div > img")
			captcha_src = captcha_image.get_attribute("src")
			return captcha_src
		except Exception as e:
			print("Капча отсутствует или не удалось получить:", str(e)[:100])
			return None # TODO Ошибка подразумевает, что капчи нет. Однако, капча может возвращаться по другому селектору

	def get_local_storage(self):
		local_storage = self.driver.execute_script("return window.localStorage;")
		return local_storage

	def init_driver(self):
		service = Service()
		options = webdriver.ChromeOptions()
		options.add_argument('--no-sandbox')
		options.add_argument('--disable-gpu')
		options.add_argument(f'--user-data-dir={self.sessions_path}/sessions_driver/{self.phone_number}')
		
		if self.headless:
			options.add_argument('--headless')

		self.driver = webdriver.Chrome(service=service, options=options)
		self.driver.set_window_size(400, 800)

	def request_code(self):
		"""
		Возвращает капчу в base64 либо None в случае отсутствии капчи
		"""
		self.init_driver()

		url = "https://www.wildberries.ru/security/login?returnUrl=https%3A%2F%2Fwww.wildberries.ru%2Flk%2Fbasket"
		self.driver.get(url)

		xpath = '//*[@id="spaAuthForm"]/div/div[1]/div/div[2]/input'
		phone_element = WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.XPATH, xpath))
		)
		phone_element.click()
		phone_element.send_keys(self.wb_input_phone)
		phone_element.send_keys(Keys.RETURN)

		time.sleep(2)
		
		captcha: Optional[str] = self.check_captcha_exist()

		if captcha:
			return {
				#"value":{
				#	"captchaData":{
				#		"imageSrc":captcha # Для старых вызовов
				#	},
				#	"instanceId":{}
				#},
				"captcha_base64": captcha # Для новых вызовов
			}
		
		return None

	def confirm_captcha(self, captcha_code=None, *args, **kwargs):
		captcha_code = captcha_code or input("Введите капчу: ")
		selector = "#smsCaptchaCode"
		captcha_input = self.driver.find_element(By.CSS_SELECTOR, selector)
		captcha_input.send_keys(captcha_code)

		time.sleep(2)

		captcha_exist = self.check_captcha_exist()
		if captcha_exist:
			return False
		
		return True

	def signin(self, code=None):
		if self.debug_mode:
			self.driver.save_screenshot(f'{time.time()}.png')
		code = code or input("Введите код подтверждения: ")
		selector = "#spaAuthForm > div > div.login__code.form-block > div > input:nth-child(5)"
		sms_input = self.driver.find_element(By.CSS_SELECTOR, selector)
		sms_input.send_keys(code)

		time.sleep(3)
		cookies_dict = self._convert_cookies_to_dict_for_pickle(self.driver)

		local_storage = self.get_local_storage()
		bearer_token = json.loads(local_storage['wbx__tokenData'])['token']
		cookies_dict['authorization'] = "Bearer " + bearer_token

		self.session.cookies.update(cookies_dict)
		self._save_session()
		return True
