import pickle
import requests

from .authentication import WBSellerAuth
from .exceptions import WildberriesError


class WBSellerAuth(WBSellerAuth):
    def __init__(self, phone_number=None, session_file=None, sessions_folder="wb_sessions", headless=True, initialize_driver=False, request_proxy=None):
        super().__init__(phone_number, session_file, sessions_folder, headless, initialize_driver)

        self.request_proxy = request_proxy
        if self.request_proxy and not self.request_proxy.startswith("http://"):
            raise Exception("request_proxy must startswith http://")


class SellerInternalAPI(WBSellerAuth):

    def __init__(self, *args, **kwargs) -> None:
        """	Авторизация/вход в сессию
        :param phone_number: Номер телефона без "+"
        :param path_session: Относительный путь к папке с сессиями
        :raise invalid_phone_number: Некорректный номер телефона
        :raise too_many_attempts: Большое количество попыток. Необходимо подождать.
        """

        super().__init__(*args, **kwargs)
        self.session = requests.Session()
        self._load_session()

    def _load_session(self):
        with open(self.session_file, "rb") as f:
            self.session.cookies = pickle.load(f)

    def get_user_suppliers(self) -> list:
        """ Получить всех поставщиков
        Требуется авторизация
        :return dict: Возвращает данные о доступных поставщиках
        """

        url = "https://seller.wildberries.ru/ns/suppliers/suppliers-portal-core/suppliers"
        body = [{
            "method": "getUserSuppliers",
            "params": {},
            "id": "json-rpc_3",
            "jsonrpc": "2.0"
        }, {
            "method": "listCountries",
            "params": {},
            "id": "json-rpc_4",
            "jsonrpc": "2.0"
        }]
        headers = {
            'Accept': '*/*',
            'Accept-Language': 'ru-RU,ru;q=0.9',
            'Connection': 'keep-alive',
            'Content-type': 'application/json',
            'Origin': 'https://seller.wildberries.ru',
            'Referer': 'https://seller.wildberries.ru/login/ru/?redirect_url=/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
            'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        response = self.session.post(url, json=body, headers=headers, proxies=proxies)
        # print('Response', response.status_code)
        response = response.json()
        try:
            return response[0]['result']['suppliers']
        except:
            return response[1]['result']['suppliers']

    def logoff(self):
        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-length': '0',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Google Chrome";v="129", "Not=A?Brand";v="8", "Chromium";v="129"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36',
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-auth.wildberries.ru/auth/v2/auth/logoff"
        response = self.session.post(url, headers=headers, proxies=proxies)
        return response.text

    def logout(self):
        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json, application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Google Chrome";v="129", "Not=A?Brand";v="8", "Chromium";v="129"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36',
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-communications.wildberries.ru/passport/api/v2/auth/logout"
        response = self.session.post(url, json={}, headers=headers, proxies=proxies)
        return response.text


class AnalyticAPI(SellerInternalAPI):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def content_analytics_report_table_collapsed(
        self, supplier_id, start_date, end_date, limit=50, offset=0
    ):
        """
        Получить данные аналитики по товарам из ЛК

        https://seller.wildberries.ru/content-analytics/interactive-report
        """

        body = {
            "subjects": [],
            "brands": [],
            "tagIds": [],
            "nms": [],
            "start": start_date,
            "end": end_date,
            "limit": limit,
            "offset": offset,
            "orderBy": {"field": "orders", "mode": "desc"},
            "skipDeletedNm": False,
        }
        headers = {
            "accept": "*/*",
            "accept-language": "ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6",
            "content-type": "application/json",
            "origin": "https://seller.wildberries.ru",
            "referer": "https://seller.wildberries.ru/",
            "sec-ch-ua": '"Google Chrome";v="123", "Not:A-Brand";v="8", "Chromium";v="123"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Linux"',
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-site",
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36",
        }
        self.session.cookies.set(
            "x-supplier-id-external", supplier_id, 
        )
        self.session.cookies.set("locale", "ru")

        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-content.wildberries.ru/ns/analytics-api/content-analytics/api/v1/report/table-collapsed"
        response = self.session.post(url, json=body, headers=headers, proxies=proxies)
        print(response, response.text)
        return response.json()

    def content_analytics_search_phrase(
        self, supplier_id, start_date, end_date, limit=50, offset=0
    ):
        """
        Получить данные аналитики по товарам из ЛК поисковые запросы (для платных тарифов)

        https://seller.wildberries.ru/content-analytics/search-phrase
        """

        body = {
            "subjects": [],
            "brands": [],
            "tagIds": [],
            "nms": [],
            "start": start_date,
            "end": end_date,
            "limit": limit,
            "offset": offset,
            "orderBy": {"field": "openCard", "mode": "desc"},
            "skipDeletedNm": False,
        }
        headers = {
            "accept": "*/*",
            "accept-language": "ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6",
            "content-type": "application/json",
            "origin": "https://seller.wildberries.ru",
            "referer": "https://seller.wildberries.ru/",
            "sec-ch-ua": '"Google Chrome";v="123", "Not:A-Brand";v="8", "Chromium";v="123"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Linux"',
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-site",
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36",
        }
        self.session.cookies.set(
            "x-supplier-id-external", supplier_id, 
        )
        self.session.cookies.set("locale", "ru")

        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None

        url = "https://seller-content.wildberries.ru/ns/analytics-api/content-analytics/api/v1/search-report/detail"
        response = self.session.post(url, json=body, headers=headers, proxies=proxies)
        return response.json()


    def consolidated_analytics_back_consolidated_table(self, supplier_id):
        """
        Получить сводную таблицу по продавцу

        https://seller.wildberries.ru/analytics/summary-report
        """

        self.session.cookies.set(
            "x-supplier-id-external", supplier_id, 
        )

        self.session.cookies.set("locale", "ru")

        headers = {
            "Accept": "*/*",
            "Accept-Language": "ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6",
            "Connection": "keep-alive",
            "Content-type": "application/json",
            "Referer": "https://seller.wildberries.ru/",
            "Sec-Fetch-Dest": "empty",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "same-origin",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36",
            "sec-ch-ua": '"Google Chrome";v="117", "Not;A=Brand";v="8", "Chromium";v="117"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Linux"',
        }
        url = "https://seller-weekly-report.wildberries.ru/ns/consolidated/analytics-back/api/v1/consolidated-table?isCommission=2"
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        response = self.session.get(url, headers=headers, proxies=proxies)
        if not response:
            raise Exception(response.text)
        return response.json()

    def seller_wb_balance_report(self, supplier_id, limit=31, date_from=None, date_to=None):
        """
        Финансовый отчет

        https://seller.wildberries.ru/suppliers-mutual-settlements/reports-implementations/reports-daily
        """

        self.session.cookies.set(
            "x-supplier-id-external", supplier_id, 
        )
        # self.session.cookies.set("x-supplier-id", supplier_id, domain=".wildberries.ru")

        self.session.cookies.set("locale", "ru")

        headers = {
            "authority": "seller-weekly-report.wildberries.ru",
            "accept": "*/*",
            "accept-language": "ru,en;q=0.9,fr;q=0.8",
            "content-type": "application/json",
            "origin": "https://seller.wildberries.ru",
            "referer": "https://seller.wildberries.ru/",
            "sec-ch-ua": '"Not_A Brand";v="8", "Chromium";v="120", "YaBrowser";v="24.1", "Yowser";v="2.5"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Windows"',
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-site",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 YaBrowser/24.1.0.0 Safari/537.36",
        }
        url = f"https://seller-weekly-report.wildberries.ru/ns/reports/seller-wb-balance/api/v1/reports?dateFrom=&dateTo=&limit={limit}&searchBy=&skip=0&type=5"
        if date_from:
            url += f"&dateFrom={date_from}&dateTo={date_to}"
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        response = self.session.get(url, headers=headers, proxies=proxies)
        if not response:
            raise Exception(response.text)
        return response.json()

    def content_analytics_report_table_detail(
            self, start_date, end_date, limit=50, offset=0
        ):
        """
        Получить данные аналитики по товарам из ЛК

        https://seller.wildberries.ru/content-analytics/interactive-report
        """

        body = {
            "subjects": [],
            "brands": [],
            "tagIds": [],
            "nms": [],
            "start": start_date,
            "end": end_date,
            "limit": limit,
            "offset": offset,
            "orderBy": {"field": "orders", "mode": "desc"},
            "skipDeletedNm": False,
        }
        headers = {
            "accept": "*/*",
            "accept-language": "ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6",
            "content-type": "application/json",
            "origin": "https://seller.wildberries.ru",
            "referer": "https://seller.wildberries.ru/",
            "sec-ch-ua": '"Google Chrome";v="123", "Not:A-Brand";v="8", "Chromium";v="123"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Linux"',
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-site",
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36",
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-content.wildberries.ru/ns/analytics-api/content-analytics/api/v1/report/table-detail"
        response = self.session.post(url, json=body, headers=headers, proxies=proxies)
        return response.json()


class CPMAPI(SellerInternalAPI):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def cpm_api_v5_upd(self, supplier_id, pageNumber=1, pageSize=10000, from_date=None, to_date=None):
        """
        Получить данные о финансах

        https://cmp.wildberries.ru/campaigns/finances
        """

        self.session.cookies.set(
            "x-supplier-id", supplier_id
        )
        self.session.cookies.set(
            "x-supplier-id-external", supplier_id
        )

        headers = {
            "accept": "*/*",
            "accept-language": "ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6",
            "referer": "https://cmp.wildberries.ru/campaigns/finances",
            "sec-ch-ua": '"Google Chrome";v="123", "Not:A-Brand";v="8", "Chromium";v="123"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Linux"',
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36",
        }
        url = f"https://cmp.wildberries.ru/api/v5/upd?pageNumber={pageNumber}&pageSize={pageSize}&type=%5B4%2C5%2C6%2C7%2C8%2C9%5D&from={from_date}&to={to_date}"
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        response = self.session.get(url, headers=headers, proxies=proxies)
        print(response)
        return response.json()


class FinancialReportAPI(SellerInternalAPI):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def reports_weekly_list(self):
        """
        Получить список еженедельных отчетов
        """

        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Chromium";v="124", "Google Chrome";v="124", "Not-A.Brand";v="99"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36',
        }
        url = 'https://seller-services.wildberries.ru/ns/reports/seller-wb-balance/api/v1/reports-weekly?dateFrom=&dateTo=&limit=25&searchBy=&skip=0&type=6'
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        response = self.session.get(url, headers=headers, proxies=proxies)
        # print(response.text)
        if not response:
            raise WildberriesError(response.text)
        return response.json()

    def reports_weekly_archived_excel(self, report_id):
        """
        Получить zip excel отчет по id
        """

        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Chromium";v="124", "Google Chrome";v="124", "Not-A.Brand";v="99"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36',
        }
        url = f"https://seller-services.wildberries.ru/ns/reports/seller-wb-balance/api/v1/reports-weekly/{report_id}/details/archived-excel"
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        response = self.session.get(url, headers=headers, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()


class IncomesAPI(SellerInternalAPI):
    """ API поставок """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def acceptanceCoefficientsReport(self, dateFrom, dateTo):
        """ Тарифы складов """

        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
        }
        data = {
            "params": {
                "dateTo": dateTo, # "2024-09-12T21:00:00.000Z",
                "dateFrom": dateFrom, # "2024-08-29T11:53:44.641Z"
            },
            "jsonrpc": "2.0",
            "id": "json-rpc_18"
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-supply.wildberries.ru/ns/sm-supply/supply-manager/api/v1/supply/acceptanceCoefficientsReport"
        response = self.session.post(url, headers=headers, json=data, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()

    def draftCreate(self):
        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
        }
        data = {
            "params": {},
            "jsonrpc": "2.0",
            "id": "json-rpc_23"
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-supply.wildberries.ru/ns/sm-draft/supply-manager/api/v1/draft/create"
        response = self.session.post(url, headers=headers, json=data, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()

    def removeGoods(self, barcodes, draftID):
        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
        }
        data = {
            "params": {
                "barcodes": barcodes,
                "draftID": draftID
            },
            "jsonrpc": "2.0",
            "id": "json-rpc_32"
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-supply.wildberries.ru/ns/sm-draft/supply-manager/api/v1/draft/removeGoods"
        response = self.session.post(url, headers=headers, json=data, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()

    def updateDraftGoods(self, barcodes, draftID):
        """
        barcode - [{"barcode": "2000092488067","quantity": 1}]
        """

        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
        }
        data = {
            "params": {
                "barcodes": barcodes,
                "draftID": draftID,
            },
            "jsonrpc": "2.0",
            "id": "json-rpc_31"
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-supply.wildberries.ru/ns/sm-draft/supply-manager/api/v1/draft/UpdateDraftGoods"
        response = self.session.post(url, headers=headers, json=data, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()

    def listDraftGoods(self, draftID, limit=15, offset=1, search=""):
        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9'
            'content-type;: ;application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
        }
        data = {
            "params": {
                "draftID": draftID,
                "limit": limit,
                "offset": offset,
                "filter": {
                    "orderBy": {
                        "barcode": 1
                    },
                    "search": search,
                }
            },
            "jsonrpc": "2.0",
            "id": "json-rpc_33"
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-supply.wildberries.ru/ns/sm-draft/supply-manager/api/v1/draft/listDraftGoods"
        response = self.session.post(url, headers=headers, json=data, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()

    def listSupplies(self, pageNumber=1, pageSize=20, sortBy="createDate", sortDirection="desc", statusId=-2):
        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
        }
        data = {
            "params": {
                "pageNumber": pageNumber,
                "pageSize": pageSize,
                "sortBy": sortBy,
                "sortDirection": sortDirection,
                "statusId": statusId,
            },
            "jsonrpc": "2.0",
            "id": "json-rpc_23"
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-supply.wildberries.ru/ns/sm-supply/supply-manager/api/v1/supply/listSupplies"
        response = self.session.post(url, headers=headers, json=data, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()

    def supplyDetails(self, preorderID=None, supplyID=None, pageNumber=1, pageSize=20, search=""):
        """
        preorderID - номер заказа
        supplyID - номер поставки
        """

        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
        }
        data = {
            "params": {
                "pageNumber": pageNumber,
                "pageSize": pageSize,
                "preorderID": preorderID,
                "search": search,
                "supplyID": supplyID,
            },
            "jsonrpc": "2.0",
            "id": "json-rpc_27"
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-supply.wildberries.ru/ns/sm-supply/supply-manager/api/v1/supply/supplyDetails"
        response = self.session.post(url, headers=headers, json=data, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()

    def getAcceptanceCosts(self, dateFrom, dateTo, preorderID=None, supplyId=None):
        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
        }
        data = {
            "params": {
                "dateFrom": dateFrom,  # "2024-08-30T11:36:57.947Z",
                "dateTo": dateTo,  # "2024-09-28T21:00:00.000Z",
                "preorderID": preorderID,
                "supplyId": supplyId,
            },
            "jsonrpc": "2.0",
            "id": "json-rpc_34"
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-supply.wildberries.ru/ns/sm-supply/supply-manager/api/v1/supply/getAcceptanceCosts"
        response = self.session.post(url, headers=headers, json=data, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()

    def plan_add(self, preOrderId, deliveryDate, supplierAssignUUID=None):
        """ Запланировать поставку """

        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
        }
        data = {
            "params": {
                "preOrderId": preOrderId,
                "deliveryDate": deliveryDate,  # "2024-09-11T00:00:00Z",
                "supplierAssignUUID": supplierAssignUUID,
            },
            "jsonrpc": "2.0",
            "id": "json-rpc_108"
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-supply.wildberries.ru/ns/sm/supply-manager/api/v1/plan/add"
        response = self.session.post(url, headers=headers, json=data, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()

    def plan_update(self, supplyId, deliveryDate):
        """ Перенести поставку """

        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
        }
        data = {
            "params": {
                "supplyId": supplyId,
                "deliveryDate": deliveryDate,
            },
            "jsonrpc": "2.0",
            "id": "json-rpc_127"
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-supply.wildberries.ru/ns/sm/supply-manager/api/v1/plan/update"
        response = self.session.post(url, headers=headers, json=data, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()

    def supply_delete(self, supplyId):
        """ Удалить поставку """

        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'content-type': 'application/json',
            'origin': 'https://seller.wildberries.ru',
            'priority': 'u=1, i',
            'referer': 'https://seller.wildberries.ru/',
            'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
        }
        data = {
            "params": {
                "supplyId": supplyId,
            }, 
            "jsonrpc": "2.0", 
            "id":"json-rpc_27"
        }
        proxies = {
            "http": self.request_proxy,
            "https": self.request_proxy,
        } if self.request_proxy else None
        url = "https://seller-supply.wildberries.ru/ns/sm-supply/supply-manager/api/v1/supply/delete"
        response = self.session.post(url, headers=headers, json=data, proxies=proxies)
        if not response:
            raise WildberriesError(response.text)
        return response.json()
