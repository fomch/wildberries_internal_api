import os
import io
import json
import time
import base64
import pickle
import warnings
import urllib.parse
from typing import Optional

import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service

from .authentication import WBBuyerAuth
from .exceptions import WildberriesError


class WBBuyer(WBBuyerAuth):

	def __init__(self, phone_number=None, sessions_path="sessionsfl", headless=True, debug_mode=False):		
		super().__init__(phone_number, sessions_path, headless, debug_mode)

	def _make_request(self, method="POST", json_data={}, url=None):

		if method == "POST":
			response = self.session.post(url, json=json_data, headers=self.default_headers)
		if method == "GET":
			response = self.session.get(url, params=json_data, headers=self.default_headers)

		return response

	def get_notifications(self):
		"""
		Получить уведомления пользователя (коды авторизации)
		"""

		url = 'https://www.wildberries.ru/webapi/lk/newsfeed/events/data?'
		res = self._make_request("POST", url=url)
		return res.json()
	
	def get_myorders_delivery_active(self):
		"""
		Получить инфу о заказах
		"""
		url = 'https://www.wildberries.ru/webapi/v2/lk/myorders/delivery/active'
		res = self._make_request("POST", url=url)
		return res.json()

	def get_myorders_archive(self):
		"""
		Получить активные покупки
		"""

		headers = {
			'authority': 'www.wildberries.ru',
			'accept': '*/*',
			'accept-language': 'ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6',
			'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'origin': 'https://www.wildberries.ru',
			'referer': 'https://www.wildberries.ru/lk/myorders/archive',
			'sec-ch-ua': '"Chromium";v="116", "Not)A;Brand";v="24", "Google Chrome";v="116"',
			'sec-ch-ua-mobile': '?0',
			'sec-ch-ua-platform': '"Linux"',
			'sec-fetch-dest': 'empty',
			'sec-fetch-mode': 'cors',
			'sec-fetch-site': 'same-origin',
			'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36',
			'x-requested-with': 'XMLHttpRequest',
			'x-spa-version': '9.3.127.1',
		}
		url = 'https://www.wildberries.ru/webapi/lk/myorders/archive/get'
		res = self.session.post(url, headers=headers)
		return res.json()

	def get_qr_code(self):
		"""
		Получить QR код для доставок
		"""
		self.init_driver()
		self.driver.get('https://www.wildberries.ru/lk/myorders/delivery')
		time.sleep(5)
		# self.driver.save_screenshot(f'{time.time()}.png')

		qr_code_element = WebDriverWait(self.driver, 30).until(
			EC.visibility_of_element_located((By.CLASS_NAME, "delivery-qr__code"))
		)

		code_element = WebDriverWait(self.driver, 10).until(
			EC.visibility_of_element_located((By.CLASS_NAME, "delivery-qr__text"))
		)

		qrCode = qr_code_element.get_attribute("src")
		privateCode = code_element.text

		self.driver.close()

		value = {
			'value': {
				'qrCode': qrCode[22:],
				'privateCode': privateCode
			}
		}

		return value
