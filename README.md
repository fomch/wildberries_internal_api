# Внутреннее API Wildberries на Python

## Установка

```
pip install -U git+https://gitlab.com/fomch/wildberries_internal_api
```

## Авторизация

```python
from wildberries_internal_api import WBSellerAuth

wb = WBSellerAuth("7999...")
if not wb.check_session():
    try:
        captcha = wb.send_number_phone()
    except Exception as e:
        captcha = None
    if captcha:
        wb.send_captcha()
    wb.send_verify_code()
```

## Пример использования

```python
from wildberries_internal_api import AnalyticAPI

analytic_api = AnalyticAPI("<phone_number>")
sppls = analytic_api.get_user_suppliers()
print(analytic_api.seller_wb_balance_report(sppls[0]["id"])) 
```
